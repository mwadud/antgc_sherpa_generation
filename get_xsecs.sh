#!/bin/bash

channel=llg
version=9Oct2018

jobdir=/eos/uscms/store/user/mwadud/aNTGC/crab/



h3z=( 0.0038 0.0029 0.0015 0.0008 0 )
# h3z=( -0.0038 -0.0029 -0.0015 -0.0008 )
ptmin=( 10 50 80 100 200 300 400 500 800 )
ptmax=( 50 80 100 200 300 400 500 800 1000 )

nsamples=100
nevents=10000



for j in {2..8}
do
	ptmin_val=${ptmin[${j}]}
	ptmax_val=${ptmax[${j}]}

	echo "pt bin : "${ptmin_val}"-"${ptmax_val} "GeV"

	# for i in {0..3}
	# for i in 4
	# do
	i=4
		h3z_val=${h3z[${i}]}
		echo "	" $h3z_val

		couplingname=h3z${h3z[${i}]}_ptmin${ptmin_val}_ptmax${ptmax_val}
		couplingname=$(sed "s/\./p/g" <<< ${couplingname})
		coupling_job_dir=${jobdir}/aNTGC_${couplingname}_llg_${version}/crab_${couplingname}_llg

		logdir=$(find ${coupling_job_dir} -maxdepth 3 -type d -name "log" )

		echo ${logdir}
		
		xsecfile=./xsecs_${couplingname}.txt

		rm -rf ${xsecfile}

		for i in {1..200}
		do
			compressedfile=${logdir}/cmsRun_${i}.log.tar.gz
			
			if [ ! -e ${compressedfile} ];
			then
				continue
			fi

			tar -xf ${compressedfile} -C ${logdir}

			logfile=${logdir}/cmsRun-stdout-${i}.log

			if ( ! grep -q "After filter: final cross section =" ${logfile} )
			then
				continue
			fi

			xsec=$(grep "After filter: final cross section =" ${logfile})
			xsec=$(echo $xsec | sed "s/After filter: final cross section =//g")
			xsec=$(echo $xsec | sed "s/+-/,/g")
			xsec=$(echo $xsec | sed "s/ //g")
			xsec=$(echo $xsec | sed "s/nb/,nb/g")
			xsec=$(echo $xsec | sed "s/pb/,pb/g")
			xsec=$(echo $xsec | sed "s/fb/,fb/g")

			echo $xsec >> ${xsecfile}

			echo "       $i   " $xsec
		# done

	done
done

echo "Task complete!"
