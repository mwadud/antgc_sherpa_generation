#!/bin/bash

compressedfile=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0_ptmin80_ptmax100/crab_h3z0_ptmin80_ptmax100_llg/results/cmsRun_2.log.tar.gz
cd /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0_ptmin80_ptmax100/crab_h3z0_ptmin80_ptmax100_llg/results/
tar -xvf ${compressedfile}

logfile=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0_ptmin80_ptmax100/crab_h3z0_ptmin80_ptmax100_llg/results/cmsRun-stdout-2.log

xsec=$(grep "After filter: final cross section =" ${logfile})

xsec=$(echo $xsec | sed "s/After filter: final cross section =//g")
xsec=$(echo $xsec | sed "s/+-/,/g")
xsec=$(echo $xsec | sed "s/ //g")
xsec=$(echo $xsec | sed "s/nb/,nb/g")
xsec=$(echo $xsec | sed "s/pb/,pb/g")
xsec=$(echo $xsec | sed "s/fb/,fb/g")


echo "xsec is " $xsec

echo "Task complete!"
