#!/bin/bash

channel=jjg
step=1

jobdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/${channel}
# sherpaDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/aNTGCeventGeneration_mine/test/
# cmsswDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/Configuration/Generator/python/
submitdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/

crab_cfg_template=${submitdir}/crab_submit.py



h3z=( 0 0.0038 0.0029 0.0015 0.0008 )
# h3z=( -1 -0.0038 -0.0029 -0.0015 -0.0008 )
ptmin=( 0 10 50 80 100 200 300 400 500 800)
ptmax=( 10 50 80 100 200 300 400 500 800 1000)

nsamples=100
nevents=10000



for j in {0..8}
#for j in 8
do
	ptmin=${ptmin[${j}]}
	ptmax=${ptmax[${j}]}

	echo "pt bin : "${ptmin}"-"${ptmax} "GeV"

	for i in {0..4}
	# for i in `seq 0 10`
	#for i in 4
	do
		h3z_val=${h3z[${i}]}
		echo $h3z_val

		if [ $i -eq 3 ] && [ $j -eq 2 ]; then
			continue
		fi
		
#		if [ $i -le 2 ] && [ $j -eq 4 ]; then
#			continue
#		fi

		couplingname=h3z${h3z_val}_ptmin${ptmin}_ptmax${ptmax}
		couplingname=$(sed "s/\./p/g" <<< ${couplingname})
		coupling_job_dir=${jobdir}/${couplingname}/
		crab_cfg_file=${coupling_job_dir}/crab_submit_step${step}_${couplingname}_${channel}.py

		psetname=sherpa_${couplingname}_step${step}_cff.py
		
		jobname=${couplingname}_${channel}

		cp ${crab_cfg_template} ${crab_cfg_file}
		sed -i 's|#workarea|'$coupling_job_dir'|g' ${crab_cfg_file}
		sed -i 's|#jobname|'$jobname'|g' ${crab_cfg_file}
		sed -i 's|#psetname|'$psetname'|g' ${crab_cfg_file}
		sed -i 's|#couplingname|'$couplingname'|g' ${crab_cfg_file}

		cd ${coupling_job_dir}

		python ${crab_cfg_file}

		cd -

		#echo "Submitted" ${couplingname}
	done
done

echo "Submission complete!"
