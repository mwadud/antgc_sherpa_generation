#!/bin/bash

channel=jjg

jobdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/${channel}/
sherpaDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/

cmsswDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/Configuration/Generator/python/
# filedir=/eos/cms/store/user/mwadud/aNTGC/signal/llG/1000/
submitdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/

# runcardtemplate=NTCG_NLO_llg.dat
# runcardtemplate=NTGC_LO_llg.dat
runcardtemplate=NTGC_LO_jjg.dat

h3z=( 0 0.0038 0.0029 0.0015 0.0008 )
# h3z=( -1 -0.0038 -0.0029 -0.0015 -0.0008 )
ptmin=( 1 10 50 80 100 200 300 400 500 800)
ptmax=( 10 50 80 100 200 300 400 500 800 1000)


nsamples=100
nevents=10000

mkdir -p ${jobdir}

echo "Making SHERPACKs..."

# for j in {0..9}
for j in 0
do
	ptmin_val=${ptmin[${j}]}
	ptmax_val=${ptmax[${j}]}

	echo "pt bin : "${ptmin_val}"-"${ptmax_val} "GeV"

	for i in {0..4}
	do
		#if [ $i -ne 2 ] || [ $j -ne 3 ]; then
		#continue
		#fi

		h3z_val=${h3z[${i}]}
		jobname=h3z${h3z_val}_ptmin${ptmin_val}_ptmax${ptmax_val}
		couplingname=$(sed "s/\./p/g" <<< ${jobname})
		coupling_job_dir=${jobdir}/${couplingname}/
		scriptfile=${coupling_job_dir}generate_SHERPACK_${couplingname}.sh	
		scripttemplate=${submitdir}/generate_SHERPACK.sh

		echo "SHERPACK for " ${couplingname}

#		rm -rf ${coupling_job_dir}

		mkdir -p ${coupling_job_dir}log/

		cp ${sherpaDir}MakeSherpaLibs.sh ${coupling_job_dir}
		cp ${sherpaDir}PrepareSherpaLibs.sh ${coupling_job_dir}

		cp ${scripttemplate} ${scriptfile}
		sed -i 's|#sherpaDir|'$sherpaDir'|g' ${scriptfile}
		sed -i 's|#jobdir|'${jobdir}'|g' ${scriptfile}
		sed -i 's|#cmsswDir|'$cmsswDir'|g' ${scriptfile}
		sed -i 's|#submitdir|'$submitdir'|g' ${scriptfile}
		sed -i 's|#runcardtemplate|'$runcardtemplate'|g' ${scriptfile}
		sed -i 's|#h3z|'$h3z_val'|g' ${scriptfile}
		sed -i 's|#ptmin|'$ptmin_val'|g' ${scriptfile}
		sed -i 's|#ptmax|'$ptmax_val'|g' ${scriptfile}
		sed -i 's|#workdir|'$coupling_job_dir'|g' ${scriptfile}
		sed -i 's|#newname|'$couplingname'|g' ${scriptfile} 
		sed -i 's|#_nevents|'${nevents}'|g' ${scriptfile}

		chmod +x ${scriptfile}

		bsub -R "pool>20" -q 1nh -o ${coupling_job_dir}log/ -J ${couplingname} < ${scriptfile} &

	done
done

echo "Submission complete!"
