from CRABClient.UserUtilities import config, getUsernameFromSiteDB
import sys

config = config()


#**************************submit function***********************
from CRABAPI.RawCommand import crabCommand
from CRABClient.ClientExceptions import ClientException
from httplib import HTTPException
def submit(config):
    try:
        crabCommand('submit', config = config)
    except HTTPException as hte:
        print "Failed submitting task: %s" % (hte.headers)
    except ClientException as cle:
        print "Failed submitting task: %s" % (cle)
#****************************************************************


version='24Oct2018'

workarea='#workarea'
jobName='#jobname'
mainOutputDir = '/store/user/mwadud/aNTGC/crab/'


config.General.requestName = '#jobname'
config.General.transferLogs = True
config.General.workArea = '%s' % workarea

config.Site.storageSite = 'T3_US_FNALLPC'
#config.Site.storageSite = 'T2_CH_CERN'
config.Site.blacklist = ['T2_US_Vanderbilt']
#config.Site.whitelist = ["T2_CH_CERN",'T2_IT_Bari', 'T2_DE_DESY', 'T2_IT_Legnaro']
config.Site.whitelist = ["T2_US*"]


config.JobType.psetName  = '#psetname'
config.JobType.pluginName  = '#pluginname'
config.JobType.maxMemoryMB = 4000
#config.JobType.eventsPerLumi = 10
config.JobType.sendExternalFolder     = True
config.JobType.inputFiles = [ '%s/sherpa_#couplingname_MASTER.md5'% workarea, '%s/sherpa_#couplingname_MASTER.tgz' % workarea]


config.Data.inputDBS = '#inputdbs'
#config.Data.inputDataset = '#inputDataset'
config.Data.publication = True
#config.Data.outputPrimaryDataset='aNTGC_#jobname_%s' % version
config.Data.outputDatasetTag ='aNTGC_#jobname_%s' % version
config.Data.allowNonValidInputDataset = True
config.Data.outLFNDirBase = '%s' % mainOutputDir
config.Data.splitting     = '#splitting'
config.Data.unitsPerJob   = 200
config.Data.totalUnits = 10000
config.Data.ignoreLocality = True

submit(config)
