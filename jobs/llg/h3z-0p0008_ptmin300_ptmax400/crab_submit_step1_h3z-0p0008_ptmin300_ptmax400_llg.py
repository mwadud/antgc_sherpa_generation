from CRABClient.UserUtilities import config, getUsernameFromSiteDB
import sys

config = config()


#**************************submit function***********************
from CRABAPI.RawCommand import crabCommand
from CRABClient.ClientExceptions import ClientException
from httplib import HTTPException
def submit(config):
    try:
        crabCommand('submit', config = config)
    except HTTPException as hte:
        print "Failed submitting task: %s" % (hte.headers)
    except ClientException as cle:
        print "Failed submitting task: %s" % (cle)
#****************************************************************


version='10Oct2018'

workarea='/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z-0p0008_ptmin300_ptmax400/'
jobName='h3z-0p0008_ptmin300_ptmax400_llg'
mainOutputDir = '/store/user/mwadud/aNTGC/crab/'


config.General.requestName = 'h3z-0p0008_ptmin300_ptmax400_llg'
config.General.transferLogs = True
config.General.workArea = '%s' % workarea

config.Site.storageSite = 'T3_US_FNALLPC'
config.Site.blacklist = ['T2_US_Vanderbilt']
config.Site.whitelist = ["T2_US*"]


config.JobType.psetName  = 'sherpa_h3z-0p0008_ptmin300_ptmax400_step1_cff.py'
config.JobType.pluginName  = 'PrivateMC'
config.JobType.maxMemoryMB = 4000
config.JobType.eventsPerLumi = 10
config.JobType.sendExternalFolder     = True
config.JobType.inputFiles = [ '%s/sherpa_h3z-0p0008_ptmin300_ptmax400_MASTER.md5'% workarea, '%s/sherpa_h3z-0p0008_ptmin300_ptmax400_MASTER.tgz' % workarea]


config.Data.inputDBS = 'global'
#config.Data.inputDataset = '#inputDataset'
config.Data.publication = True
config.Data.outputPrimaryDataset='aNTGC_h3z-0p0008_ptmin300_ptmax400_llg_%s' % version
config.Data.outputDatasetTag ='aNTGC_h3z-0p0008_ptmin300_ptmax400_llg_%s' % version
config.Data.allowNonValidInputDataset = True
config.Data.outLFNDirBase = '%s' % mainOutputDir
config.Data.splitting     = 'EventBased'
config.Data.unitsPerJob   = 50
config.Data.totalUnits = 10000
config.Data.ignoreLocality = True

submit(config)