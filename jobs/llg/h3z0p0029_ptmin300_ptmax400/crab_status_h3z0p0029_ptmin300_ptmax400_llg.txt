CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0029_ptmin300_ptmax400/crab_h3z0p0029_ptmin300_ptmax400_llg/
Task name:			181009_192800:mwadud_crab_h3z0p0029_ptmin300_ptmax400_llg
Grid scheduler - Task Worker:	crab3@vocms0144.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181009_192800%3Amwadud_crab_h3z0p0029_ptmin300_ptmax400_llg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181009_192800%3Amwadud_crab_h3z0p0029_ptmin300_ptmax400_llg
Warning:			Task requests 12000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (200/200)

Publication status:		done          100.0% (200/200)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z0p0029_ptmin300_ptmax400_llg_9Oct2018/mwadud-crab_h3z0p0029_ptmin300_ptmax400_llg-70df460070055afb07e8acb7d869e2c1/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0029_ptmin300_ptmax400_llg_9Oct2018%2Fmwadud-crab_h3z0p0029_ptmin300_ptmax400_llg-70df460070055afb07e8acb7d869e2c1%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (12000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2013MB min, 2247MB max, 2007MB ave
 * Runtime: 0:21:46 min, 1:12:33 max, 0:40:57 ave
 * CPU eff: 54% min, 98% max, 94% ave
 * Waste: 0:26:40 (0% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0029_ptmin300_ptmax400/crab_h3z0p0029_ptmin300_ptmax400_llg/crab.log
