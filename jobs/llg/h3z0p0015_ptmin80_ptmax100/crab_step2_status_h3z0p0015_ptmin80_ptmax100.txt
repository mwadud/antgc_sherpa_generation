CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0015_ptmin80_ptmax100/crab_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM/
Task name:			181010_125453:mwadud_crab_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM
Grid scheduler - Task Worker:	crab3@vocms0194.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181010_125453%3Amwadud_crab_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181010_125453%3Amwadud_crab_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (200/200)

Publication status:		done          100.0% (200/200)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z0p0015_ptmin80_ptmax100_llg_9Oct2018/mwadud-aNTGC_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM_10Oct2018-505f2628da3055a723c8dbf3235c1019/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0015_ptmin80_ptmax100_llg_9Oct2018%2Fmwadud-aNTGC_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM_10Oct2018-505f2628da3055a723c8dbf3235c1019%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2MB min, 1747MB max, 97MB ave
 * Runtime: 0:01:57 min, 0:20:24 max, 0:03:39 ave
 * CPU eff: 14% min, 75% max, 51% ave
 * Waste: 5:19:47 (30% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0015_ptmin80_ptmax100/crab_h3z0p0015_ptmin80_ptmax100_llg_MINIAODSIM/crab.log
