CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0029_ptmin400_ptmax500/crab_h3z0p0029_ptmin400_ptmax500_llg/
Task name:			181009_193153:mwadud_crab_h3z0p0029_ptmin400_ptmax500_llg
Grid scheduler - Task Worker:	crab3@vocms0137.cern.ch - crab-prod-tw01
Status on the CRAB server:	KILLED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181009_193153%3Amwadud_crab_h3z0p0029_ptmin400_ptmax500_llg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181009_193153%3Amwadud_crab_h3z0p0029_ptmin400_ptmax500_llg
Warning:			Task requests 12000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Warning:			/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=mwadud/CN=793344/CN=Mohammad Abrar Wadud
Warning:			/DC=ch/DC=cern/OU=Organic Units/OU=Users/CN=mwadud/CN=793344/CN=Mohammad Abrar Wadud
Status on the scheduler:	SUBMITTED

Jobs status:                    finished      		  99.5% (199/200)
				killed          0.5% (  1/200)

Publication status:		done           99.5% (199/200)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted     0.5% (  1/200)

Output dataset:			/aNTGC_h3z0p0029_ptmin400_ptmax500_llg_9Oct2018/mwadud-crab_h3z0p0029_ptmin400_ptmax500_llg-e8817b896ea117bf3bc06ff8ee58bb3b/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0029_ptmin400_ptmax500_llg_9Oct2018%2Fmwadud-crab_h3z0p0029_ptmin400_ptmax500_llg-e8817b896ea117bf3bc06ff8ee58bb3b%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (12000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 0MB min, 3819MB max, 2023MB ave
 * Runtime: 0:00:00 min, 4:39:10 max, 0:50:31 ave
 * CPU eff: 29% min, 98% max, 91% ave
 * Waste: 12:15:01 (7% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0029_ptmin400_ptmax500/crab_h3z0p0029_ptmin400_ptmax500_llg/crab.log
