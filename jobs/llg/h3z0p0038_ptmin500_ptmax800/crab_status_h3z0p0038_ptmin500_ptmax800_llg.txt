CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0038_ptmin500_ptmax800/crab_h3z0p0038_ptmin500_ptmax800_llg/
Task name:			181009_193459:mwadud_crab_h3z0p0038_ptmin500_ptmax800_llg
Grid scheduler - Task Worker:	crab3@vocms0198.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181009_193459%3Amwadud_crab_h3z0p0038_ptmin500_ptmax800_llg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181009_193459%3Amwadud_crab_h3z0p0038_ptmin500_ptmax800_llg
Warning:			Task requests 12000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (200/200)

Publication status:		done          100.0% (200/200)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z0p0038_ptmin500_ptmax800_llg_9Oct2018/mwadud-crab_h3z0p0038_ptmin500_ptmax800_llg-c87578ae9a19b0b27c775219188bc1f0/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0038_ptmin500_ptmax800_llg_9Oct2018%2Fmwadud-crab_h3z0p0038_ptmin500_ptmax800_llg-c87578ae9a19b0b27c775219188bc1f0%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (12000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1941MB min, 3584MB max, 2039MB ave
 * Runtime: 0:25:23 min, 1:42:52 max, 0:52:49 ave
 * CPU eff: 69% min, 98% max, 95% ave
 * Waste: 1:52:39 (1% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0038_ptmin500_ptmax800/crab_h3z0p0038_ptmin500_ptmax800_llg/crab.log
