CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z-0p0029_ptmin300_ptmax400/crab_h3z-0p0029_ptmin300_ptmax400_llg/
Task name:			181015_040142:mwadud_crab_h3z-0p0029_ptmin300_ptmax400_llg
Grid scheduler - Task Worker:	crab3@vocms0198.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181015_040142%3Amwadud_crab_h3z-0p0029_ptmin300_ptmax400_llg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181015_040142%3Amwadud_crab_h3z-0p0029_ptmin300_ptmax400_llg
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (200/200)

Publication status:		done          100.0% (200/200)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z-0p0029_ptmin300_ptmax400_llg_10Oct2018/mwadud-aNTGC_h3z-0p0029_ptmin300_ptmax400_llg_10Oct2018-d1deffa5c87be9bb933bbd372dae359d/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z-0p0029_ptmin300_ptmax400_llg_10Oct2018%2Fmwadud-aNTGC_h3z-0p0029_ptmin300_ptmax400_llg_10Oct2018-d1deffa5c87be9bb933bbd372dae359d%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1876MB min, 2170MB max, 1947MB ave
 * Runtime: 0:26:29 min, 1:23:27 max, 0:45:18 ave
 * CPU eff: 73% min, 98% max, 95% ave
 * Waste: 3:09:55 (2% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z-0p0029_ptmin300_ptmax400/crab_h3z-0p0029_ptmin300_ptmax400_llg/crab.log
