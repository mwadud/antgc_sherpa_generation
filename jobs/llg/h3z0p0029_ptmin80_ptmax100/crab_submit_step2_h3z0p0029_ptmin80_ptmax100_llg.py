from CRABClient.UserUtilities import config, getUsernameFromSiteDB
import sys

config = config()


#**************************submit function***********************
from CRABAPI.RawCommand import crabCommand
from CRABClient.ClientExceptions import ClientException
from httplib import HTTPException
def submit(config):
    try:
        crabCommand('submit', config = config)
    except HTTPException as hte:
        print "Failed submitting task: %s" % (hte.headers)
    except ClientException as cle:
        print "Failed submitting task: %s" % (cle)
#****************************************************************


version='10Oct2018'

workarea='/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z0p0029_ptmin80_ptmax100/'
jobName='h3z0p0029_ptmin80_ptmax100_llg_MINIAODSIM'
mainOutputDir = '/store/user/mwadud/aNTGC/crab/'


config.General.requestName = 'h3z0p0029_ptmin80_ptmax100_llg_MINIAODSIM'
config.General.transferLogs = True
config.General.workArea = '%s' % workarea

config.Site.storageSite = 'T3_US_FNALLPC'
config.Site.blacklist = ['T2_US_Vanderbilt']
config.Site.whitelist = ["T2_US*"]


config.JobType.psetName  = 'sherpa_h3z0p0029_ptmin80_ptmax100_step2_cff.py'
config.JobType.pluginName  = 'Analysis'
config.JobType.maxMemoryMB = 4000
#config.JobType.eventsPerLumi = 10
config.JobType.sendExternalFolder     = True
config.JobType.inputFiles = [ '%s/sherpa_h3z0p0029_ptmin80_ptmax100_MASTER.md5'% workarea, '%s/sherpa_h3z0p0029_ptmin80_ptmax100_MASTER.tgz' % workarea]


config.Data.inputDBS = 'phys03'
config.Data.inputDataset = '/aNTGC_h3z0p0029_ptmin80_ptmax100_llg_9Oct2018/mwadud-crab_h3z0p0029_ptmin80_ptmax100_llg-ad5b7885f93b19527a90db30624c529d/USER'
config.Data.publication = True
#config.Data.outputPrimaryDataset='aNTGC_h3z0p0029_ptmin80_ptmax100_llg_MINIAODSIM_%s' % version
config.Data.outputDatasetTag ='aNTGC_h3z0p0029_ptmin80_ptmax100_llg_MINIAODSIM_%s' % version
config.Data.allowNonValidInputDataset = True
config.Data.outLFNDirBase = '%s' % mainOutputDir
config.Data.splitting     = 'EventAwareLumiBased'
config.Data.unitsPerJob   = 50
config.Data.totalUnits = 10000
config.Data.ignoreLocality = True

submit(config)
