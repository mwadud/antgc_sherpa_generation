CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z-0p0015_ptmin800_ptmax1000/crab_h3z-0p0015_ptmin800_ptmax1000_llg/
Task name:			181015_041231:mwadud_crab_h3z-0p0015_ptmin800_ptmax1000_llg
Grid scheduler - Task Worker:	crab3@vocms0121.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181015_041231%3Amwadud_crab_h3z-0p0015_ptmin800_ptmax1000_llg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181015_041231%3Amwadud_crab_h3z-0p0015_ptmin800_ptmax1000_llg
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (200/200)

Publication status:		done          100.0% (200/200)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z-0p0015_ptmin800_ptmax1000_llg_10Oct2018/mwadud-aNTGC_h3z-0p0015_ptmin800_ptmax1000_llg_10Oct2018-5c0070c03a9ad4e919eaff6df051087c/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z-0p0015_ptmin800_ptmax1000_llg_10Oct2018%2Fmwadud-aNTGC_h3z-0p0015_ptmin800_ptmax1000_llg_10Oct2018-5c0070c03a9ad4e919eaff6df051087c%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1939MB min, 3616MB max, 1955MB ave
 * Runtime: 0:37:52 min, 2:10:09 max, 1:01:47 ave
 * CPU eff: 56% min, 98% max, 96% ave
 * Waste: 0:26:38 (0% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/llg/h3z-0p0015_ptmin800_ptmax1000/crab_h3z-0p0015_ptmin800_ptmax1000_llg/crab.log
