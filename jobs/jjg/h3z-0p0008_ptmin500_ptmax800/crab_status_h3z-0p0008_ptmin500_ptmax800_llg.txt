CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0008_ptmin500_ptmax800/crab_h3z-0p0008_ptmin500_ptmax800_jjg/
Task name:			181015_104922:mwadud_crab_h3z-0p0008_ptmin500_ptmax800_jjg
Grid scheduler - Task Worker:	crab3@vocms0144.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181015_104922%3Amwadud_crab_h3z-0p0008_ptmin500_ptmax800_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181015_104922%3Amwadud_crab_h3z-0p0008_ptmin500_ptmax800_jjg
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	SUBMITTED

Jobs status:                    finished      		  95.5% (191/200)
				running         0.5% (  1/200)
				transferring    4.0% (  8/200)

Publication status:		done           95.5% (191/200)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted     4.5% (  9/200)

Output dataset:			/aNTGC_h3z-0p0008_ptmin500_ptmax800_jjg_10Oct2018/mwadud-aNTGC_h3z-0p0008_ptmin500_ptmax800_jjg_10Oct2018-29935cb1685281bb9bb24591a2257865/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z-0p0008_ptmin500_ptmax800_jjg_10Oct2018%2Fmwadud-aNTGC_h3z-0p0008_ptmin500_ptmax800_jjg_10Oct2018-29935cb1685281bb9bb24591a2257865%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2047MB min, 3657MB max, 1976MB ave
 * Runtime: 0:36:21 min, 2:20:38 max, 0:59:55 ave
 * CPU eff: 75% min, 98% max, 96% ave
 * Waste: 3:47:09 (2% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0008_ptmin500_ptmax800/crab_h3z-0p0008_ptmin500_ptmax800_jjg/crab.log
