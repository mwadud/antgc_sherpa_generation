#!/bin/bash
#export X509_USER_PROXY=/afs/cern.ch/user/m/mwadud/x509up_u91892

runcardtemplate=NTGC_LO_jjg.dat

nretry=10

globalTag=94X_mc2017_realistic_v12
jobdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/
sherpaDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/
cmsswDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/Configuration/Generator/python/
workdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg//h3z-0p0008_ptmin500_ptmax800/
submitdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/


pileupdataset=dbs:/Neutrino_E-10_gun/RunIISummer17PrePremix-MCv2_correctPU_94X_mc2017_realistic_v9-v1/GEN-SIM-DIGI-RAW

nevents=10000

float_scale=4
function float_eval()
{
    local stat=0
    local result=0.0
    if [[ $# -gt 0 ]]; then
        result=$(echo "scale=$float_scale; $*" | bc -q 2>/dev/null)
        stat=$?
        if [[ $stat -eq 0  &&  -z "$result" ]]; then stat=1; fi
    fi
    echo $result
    return $stat
}

h3z=-0.0008
ptmin=500
ptmax=800



echo "h3Z is "${h3z}
	
Mz=91.18
v=246.0
cw=0.877
sw=0.481

num=$(float_eval "(4.0*1000.0*1000.0*1000.0*1000.0*${cw}*${sw}*${h3z})")
deno=$(float_eval "(${v}*${v}*${Mz}*${Mz})")
cbtw=$(float_eval "(4.0*1000*1000*1000*1000*${cw}*${sw}*${h3z})/(${v}*${v}*${Mz}*${Mz})")


newnameZZg=h3z-0p0008_ptmin500_ptmax800
runcard=${workdir}Run.dat_${newnameZZg}
echo $newnameZZg


# set -o errexit
# exec 1>>#xsecfile
# exec 2>&1

date '+%Y-%m-%d %H:%M:%S'


source /cvmfs/cms.cern.ch/cmsset_default.sh;
cd ${cmsswDir}; eval `scramv1 runtime -sh`; cd -;

echo "num : deno "$num" " : " "$deno
echo "h3z : cbtw "${h3z}" " $cbtw

cp ${sherpaDir}Run.dat_13TeV_aNTGC_ZZg_TMP ${runcard}
cp ${submitdir}${runcardtemplate} ${runcard}
sed -i 's|'#cbtw'|'${cbtw}'|g' ${runcard}
sed -i 's|'#ptgmin'|'${ptmin}'|g' ${runcard}
sed -i 's|'#ptgmax'|'${ptmax}'|g' ${runcard}
RandSeed1=$((1 + RANDOM % 10000))
sed -i 's|'#seed1'|'${RandSeed1}'|g' ${runcard}
RandSeed2=$((1 + RANDOM % 10000))
sed -i 's|'#seed2'|'${RandSeed2}'|g' ${runcard}
RandSeed3=$((1 + RANDOM % 10000))
sed -i 's|'#seed3'|'${RandSeed3}'|g' ${runcard}
RandSeed4=$((1 + RANDOM % 10000))
sed -i 's|'#seed4'|'${RandSeed4}'|g' ${runcard}
echo "Runcard created: "${runcard}

cd ${workdir}

cat ${runcard}

echo "# of events to be generated : " ${nevents}

chmod +x MakeSherpaLibs.sh
chmod +x PrepareSherpaLibs.sh

./MakeSherpaLibs.sh -p ${newnameZZg} -o LBCR   
./PrepareSherpaLibs.sh -p ${newnameZZg} 
wait
echo "MakeSherpaLibs & PrepareSherpaLibs complete!"
wait 
cp -f ./sherpa_${newnameZZg}_MASTER_cff.py ${cmsswDir}

gensimfile=sherpa_${newnameZZg}_MASTER_cff_py_GENSIM.root
step2file=sherpa_${newnameZZg}_MASTER_cff_py_step2.root
aodsimfile=sherpa_${newnameZZg}_MASTER_cff_py_AODSIM.root
miniaodsimfile=sherpa_${newnameZZg}_MASTER_cff_py_MINIAODSIM.root

cmsDriver.py sherpa_${newnameZZg}_MASTER_cff.py --mc --step GEN,SIM,DIGI,L1,DIGI2RAW,HLT -n ${nevents} --conditions ${globalTag} --eventcontent RAWSIM --datatier GEN-SIM-RAW  --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --fileout ${gensimfile} --python_filename sherpa_${newnameZZg}_step1_cff.py --no_exec

cmsDriver.py sherpa_${newnameZZg}_MASTER_cff.py  --mc --step RAW2DIGI,RECO,RECOSIM,EI,PAT -n ${nevents} --conditions ${globalTag} --eventcontent MINIAODSIM --runUnscheduled --datatier AODSIM-MINIAODSIM --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --filein file:${gensimfile} --fileout ${miniaodsimfile} --python_filename sherpa_${newnameZZg}_step2_cff.py --no_exec

# cmsDriver.py sherpa_${newnameZZg}_MASTER_cff.py --mc -s GEN,SIM -n ${nevents} --fileout ${gensimfile} --conditions ${globalTag} --eventcontent RAWSIM --datatier GEN-SIM  --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --python_filename sherpa_${newnameZZg}_MASTER_cff.py --no_exec

# cmsDriver.py sherpa_${newnameZZg}_MASTER_cff.py --mc --step DIGI,L1,DIGI2RAW,HLT --era Run2_2017 -n ${nevents} --conditions ${globalTag} --datatier GEN-SIM-RAW --eventcontent RAWSIM --filein file:${gensimfile} --fileout ${step2file} --no_exec

# cmsDriver.py --python_filename sherpa_${newnameZZg}_MASTER_cff.py  --mc --step RAW2DIGI,RECO,RECOSIM,EI --era Run2_2017 -n ${nevents} --conditions ${globalTag} --eventcontent AODSIM runUnscheduled --datatier AODSIM --customise Configuration/DataProcessing/Utils.addMonitoring --filein file:${step2file} --fileout ${aodsimfile} --no_exec

# cmsDriver.py --python_filename sherpa_${newnameZZg}_MASTER_cff.py  --mc --step PAT --era Run2_2017 -n ${nevents} --conditions ${globalTag} --eventcontent MINIAODSIM --runUnscheduled --customise Configuration/DataProcessing/Utils.addMonitoring --datatier MINIAODSIM --filein file:${aodsimfile} --fileout ${miniaodsimfile} --no_exec


echo "Task complete!"