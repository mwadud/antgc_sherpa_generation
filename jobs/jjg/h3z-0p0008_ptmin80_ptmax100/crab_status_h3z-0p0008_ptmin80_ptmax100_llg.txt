CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0008_ptmin80_ptmax100/crab_h3z-0p0008_ptmin80_ptmax100_jjg/
Task name:			181015_103121:mwadud_crab_h3z-0p0008_ptmin80_ptmax100_jjg
Grid scheduler - Task Worker:	crab3@vocms0121.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181015_103121%3Amwadud_crab_h3z-0p0008_ptmin80_ptmax100_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181015_103121%3Amwadud_crab_h3z-0p0008_ptmin80_ptmax100_jjg
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (200/200)

Publication status:		done          100.0% (200/200)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z-0p0008_ptmin80_ptmax100_jjg_10Oct2018/mwadud-aNTGC_h3z-0p0008_ptmin80_ptmax100_jjg_10Oct2018-0b56ae82de45afe4707ab3b913d3e666/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z-0p0008_ptmin80_ptmax100_jjg_10Oct2018%2Fmwadud-aNTGC_h3z-0p0008_ptmin80_ptmax100_jjg_10Oct2018-0b56ae82de45afe4707ab3b913d3e666%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1867MB min, 2113MB max, 1912MB ave
 * Runtime: 0:16:56 min, 1:00:44 max, 0:29:19 ave
 * CPU eff: 70% min, 96% max, 92% ave
 * Waste: 0:27:14 (0% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0008_ptmin80_ptmax100/crab_h3z-0p0008_ptmin80_ptmax100_jjg/crab.log
