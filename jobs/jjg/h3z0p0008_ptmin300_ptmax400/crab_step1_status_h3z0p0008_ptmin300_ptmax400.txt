CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0008_ptmin300_ptmax400/crab_h3z0p0008_ptmin300_ptmax400_jjg/
Task name:			181024_161842:mwadud_crab_h3z0p0008_ptmin300_ptmax400_jjg
Grid scheduler - Task Worker:	crab3@vocms0194.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181024_161842%3Amwadud_crab_h3z0p0008_ptmin300_ptmax400_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181024_161842%3Amwadud_crab_h3z0p0008_ptmin300_ptmax400_jjg
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	SUBMITTED

Jobs status:                    finished      		  80.0% (40/50)
				running        18.0% ( 9/50)
				transferring    2.0% ( 1/50)

Publication status:		done           80.0% (40/50)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted    20.0% (10/50)

Output dataset:			/aNTGC_h3z0p0008_ptmin300_ptmax400_jjg_24Oct2018/mwadud-aNTGC_h3z0p0008_ptmin300_ptmax400_jjg_24Oct2018-019778684ddf8ab4e856a2df9fb767be/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0008_ptmin300_ptmax400_jjg_24Oct2018%2Fmwadud-aNTGC_h3z0p0008_ptmin300_ptmax400_jjg_24Oct2018-019778684ddf8ab4e856a2df9fb767be%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1510MB min, 2264MB max, 2132MB ave
 * Runtime: 0:05:08 min, 5:40:58 max, 3:01:08 ave
 * CPU eff: 83% min, 99% max, 97% ave
 * Waste: 2:31:02 (2% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0008_ptmin300_ptmax400/crab_h3z0p0008_ptmin300_ptmax400_jjg/crab.log
