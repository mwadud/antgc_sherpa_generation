CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0038_ptmin80_ptmax100/crab_h3z-0p0038_ptmin80_ptmax100_jjg/
Task name:			181015_102830:mwadud_crab_h3z-0p0038_ptmin80_ptmax100_jjg
Grid scheduler - Task Worker:	crab3@vocms0137.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181015_102830%3Amwadud_crab_h3z-0p0038_ptmin80_ptmax100_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181015_102830%3Amwadud_crab_h3z-0p0038_ptmin80_ptmax100_jjg
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	SUBMITTED

Jobs status:                    failed        		   0.5% (  1/200)
				finished       98.0% (196/200)
				transferring    1.5% (  3/200)

Publication status:		done           98.0% (196/200)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted     2.0% (  4/200)

Output dataset:			/aNTGC_h3z-0p0038_ptmin80_ptmax100_jjg_10Oct2018/mwadud-aNTGC_h3z-0p0038_ptmin80_ptmax100_jjg_10Oct2018-88eeece91d6e5f9a12a42dad9ee0ae7d/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z-0p0038_ptmin80_ptmax100_jjg_10Oct2018%2Fmwadud-aNTGC_h3z-0p0038_ptmin80_ptmax100_jjg_10Oct2018-88eeece91d6e5f9a12a42dad9ee0ae7d%2FUSER&instance=prod%2Fphys03

Error Summary: (use crab status --verboseErrors for details about the errors)

  1 jobs failed with exit code 2

Have a look at https://twiki.cern.ch/twiki/bin/viewauth/CMSPublic/JobExitCodes for a description of the exit codes.

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1810MB min, 2070MB max, 1903MB ave
 * Runtime: 0:12:46 min, 1:07:46 max, 0:29:58 ave
 * CPU eff: 57% min, 96% max, 91% ave
 * Waste: 5:49:04 (6% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0038_ptmin80_ptmax100/crab_h3z-0p0038_ptmin80_ptmax100_jjg/crab.log
