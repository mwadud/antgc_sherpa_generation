CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0038_ptmin100_ptmax200/crab_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM/
Task name:			181015_201458:mwadud_crab_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM
Grid scheduler - Task Worker:	crab3@vocms0120.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181015_201458%3Amwadud_crab_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181015_201458%3Amwadud_crab_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (194/194)

Publication status:		done          100.0% (194/194)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z-0p0038_ptmin100_ptmax200_jjg_10Oct2018/mwadud-aNTGC_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM_10Oct2018-eb0b3152a5410114e39f7ab94e508942/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z-0p0038_ptmin100_ptmax200_jjg_10Oct2018%2Fmwadud-aNTGC_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM_10Oct2018-eb0b3152a5410114e39f7ab94e508942%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the average jobs CPU efficiency is less than 50%, please consider to improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task

Summary of run jobs:
 * Memory: 24MB min, 1759MB max, 475MB ave
 * Runtime: 0:01:56 min, 2:44:38 max, 0:17:34 ave
 * CPU eff: 1% min, 73% max, 14% ave
 * Waste: 5:23:48 (9% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z-0p0038_ptmin100_ptmax200/crab_h3z-0p0038_ptmin100_ptmax200_jjg_MINIAODSIM/crab.log
