CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0015_ptmin80_ptmax100/crab_h3z0p0015_ptmin80_ptmax100_jjg/
Task name:			181024_160512:mwadud_crab_h3z0p0015_ptmin80_ptmax100_jjg
Grid scheduler - Task Worker:	crab3@vocms0155.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181024_160512%3Amwadud_crab_h3z0p0015_ptmin80_ptmax100_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181024_160512%3Amwadud_crab_h3z0p0015_ptmin80_ptmax100_jjg
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (50/50)

Publication status:		done          100.0% (50/50)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z0p0015_ptmin80_ptmax100_jjg_24Oct2018/mwadud-aNTGC_h3z0p0015_ptmin80_ptmax100_jjg_24Oct2018-3e1b8c592522c2b48a6773270fbbf58f/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0015_ptmin80_ptmax100_jjg_24Oct2018%2Fmwadud-aNTGC_h3z0p0015_ptmin80_ptmax100_jjg_24Oct2018-3e1b8c592522c2b48a6773270fbbf58f%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2057MB min, 3594MB max, 2155MB ave
 * Runtime: 1:01:15 min, 2:56:00 max, 1:45:54 ave
 * CPU eff: 91% min, 98% max, 97% ave
 * Waste: 0:48:52 (1% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0015_ptmin80_ptmax100/crab_h3z0p0015_ptmin80_ptmax100_jjg/crab.log
