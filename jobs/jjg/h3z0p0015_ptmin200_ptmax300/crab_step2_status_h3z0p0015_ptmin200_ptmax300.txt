CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0015_ptmin200_ptmax300/crab_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry/
Task name:			181025_094236:mwadud_crab_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry
Grid scheduler - Task Worker:	crab3@vocms0155.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181025_094236%3Amwadud_crab_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181025_094236%3Amwadud_crab_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry
Warning:			The following sites appear in both the user site blacklist and whitelist: [u'T2_US_Vanderbilt']. Since the whitelist has precedence, these sites are not considered in the blacklist.
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	COMPLETED

Jobs status:                    finished      		 100.0% (50/50)

Publication status:		done          100.0% (50/50)
(from CRAB internal bookkeeping in transferdb)

Output dataset:			/aNTGC_h3z0p0015_ptmin200_ptmax300_jjg_24Oct2018/mwadud-aNTGC_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry_24Oct2018-eb0b3152a5410114e39f7ab94e508942/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0015_ptmin200_ptmax300_jjg_24Oct2018%2Fmwadud-aNTGC_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry_24Oct2018-eb0b3152a5410114e39f7ab94e508942%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 1861MB min, 1871MB max, 1790MB ave
 * Runtime: 0:05:37 min, 0:16:34 max, 0:09:23 ave
 * CPU eff: 66% min, 85% max, 79% ave
 * Waste: 0:16:52 (3% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0015_ptmin200_ptmax300/crab_h3z0p0015_ptmin200_ptmax300_jjg_MINIAODSIM_retry/crab.log
