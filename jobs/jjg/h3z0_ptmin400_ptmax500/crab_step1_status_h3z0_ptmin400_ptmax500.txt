CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0_ptmin400_ptmax500/crab_h3z0_ptmin400_ptmax500_jjg/
Task name:			181024_161926:mwadud_crab_h3z0_ptmin400_ptmax500_jjg
Grid scheduler - Task Worker:	crab3@vocms0194.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181024_161926%3Amwadud_crab_h3z0_ptmin400_ptmax500_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181024_161926%3Amwadud_crab_h3z0_ptmin400_ptmax500_jjg
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	SUBMITTED

Jobs status:                    finished      		  98.0% (49/50)
				transferring    2.0% ( 1/50)

Publication status:		done           98.0% (49/50)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted     2.0% ( 1/50)

Output dataset:			/aNTGC_h3z0_ptmin400_ptmax500_jjg_24Oct2018/mwadud-aNTGC_h3z0_ptmin400_ptmax500_jjg_24Oct2018-4f8de5e6a94c6475fd03bbeebe0466a6/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0_ptmin400_ptmax500_jjg_24Oct2018%2Fmwadud-aNTGC_h3z0_ptmin400_ptmax500_jjg_24Oct2018-4f8de5e6a94c6475fd03bbeebe0466a6%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2296MB min, 2300MB max, 2184MB ave
 * Runtime: 1:49:43 min, 5:41:54 max, 3:10:04 ave
 * CPU eff: 93% min, 99% max, 97% ave
 * Waste: 4:43:14 (3% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0_ptmin400_ptmax500/crab_h3z0_ptmin400_ptmax500_jjg/crab.log
