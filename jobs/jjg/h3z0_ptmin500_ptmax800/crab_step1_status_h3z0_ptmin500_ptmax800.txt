CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0_ptmin500_ptmax800/crab_h3z0_ptmin500_ptmax800_jjg/
Task name:			181024_162417:mwadud_crab_h3z0_ptmin500_ptmax800_jjg
Grid scheduler - Task Worker:	crab3@vocms0144.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181024_162417%3Amwadud_crab_h3z0_ptmin500_ptmax800_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181024_162417%3Amwadud_crab_h3z0_ptmin500_ptmax800_jjg
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	SUBMITTED

Jobs status:                    finished      		  98.0% (49/50)
				transferring    2.0% ( 1/50)

Publication status:		done           98.0% (49/50)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted     2.0% ( 1/50)

Output dataset:			/aNTGC_h3z0_ptmin500_ptmax800_jjg_24Oct2018/mwadud-aNTGC_h3z0_ptmin500_ptmax800_jjg_24Oct2018-f640834ded99a767481e70cf091ee79d/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0_ptmin500_ptmax800_jjg_24Oct2018%2Fmwadud-aNTGC_h3z0_ptmin500_ptmax800_jjg_24Oct2018-f640834ded99a767481e70cf091ee79d%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Warning: the max jobs memory is less than 70% of the task requested value (4000 MB), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2152MB min, 2344MB max, 2195MB ave
 * Runtime: 2:09:28 min, 5:52:34 max, 3:33:35 ave
 * CPU eff: 94% min, 99% max, 98% ave
 * Waste: 2:06:00 (1% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0_ptmin500_ptmax800/crab_h3z0_ptmin500_ptmax800_jjg/crab.log
