from CRABClient.UserUtilities import config, getUsernameFromSiteDB
import sys

config = config()


#**************************submit function***********************
from CRABAPI.RawCommand import crabCommand
from CRABClient.ClientExceptions import ClientException
from httplib import HTTPException
def submit(config):
    try:
        crabCommand('submit', config = config)
    except HTTPException as hte:
        print "Failed submitting task: %s" % (hte.headers)
    except ClientException as cle:
        print "Failed submitting task: %s" % (cle)
#****************************************************************


version='24Oct2018'

workarea='/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0038_ptmin400_ptmax500/'
jobName='h3z0p0038_ptmin400_ptmax500_jjg_MINIAODSIM_retry'
mainOutputDir = '/store/user/mwadud/aNTGC/crab/'


config.General.requestName = 'h3z0p0038_ptmin400_ptmax500_jjg_MINIAODSIM_retry'
config.General.transferLogs = True
config.General.workArea = '%s' % workarea

config.Site.storageSite = 'T3_US_FNALLPC'
#config.Site.storageSite = 'T2_CH_CERN'
config.Site.blacklist = ['T2_US_Vanderbilt']
#config.Site.whitelist = ["T2_CH_CERN",'T2_IT_Bari', 'T2_DE_DESY', 'T2_IT_Legnaro']
config.Site.whitelist = ["T2_US*"]


config.JobType.psetName  = 'sherpa_h3z0p0038_ptmin400_ptmax500_step2_cff.py'
config.JobType.pluginName  = 'Analysis'
config.JobType.maxMemoryMB = 4000
#config.JobType.eventsPerLumi = 10
config.JobType.sendExternalFolder     = True
config.JobType.inputFiles = [ '%s/sherpa_h3z0p0038_ptmin400_ptmax500_MASTER.md5'% workarea, '%s/sherpa_h3z0p0038_ptmin400_ptmax500_MASTER.tgz' % workarea]


config.Data.inputDBS = 'phys03'
config.Data.inputDataset = '/aNTGC_h3z0p0038_ptmin400_ptmax500_jjg_24Oct2018/mwadud-aNTGC_h3z0p0038_ptmin400_ptmax500_jjg_24Oct2018-ef3a009d0f8a99ddd4d4602397c3a50d/USER'
config.Data.publication = True
#config.Data.outputPrimaryDataset='aNTGC_h3z0p0038_ptmin400_ptmax500_jjg_MINIAODSIM_retry_%s' % version
config.Data.outputDatasetTag ='aNTGC_h3z0p0038_ptmin400_ptmax500_jjg_MINIAODSIM_retry_%s' % version
config.Data.allowNonValidInputDataset = True
config.Data.outLFNDirBase = '%s' % mainOutputDir
config.Data.splitting     = 'EventAwareLumiBased'
config.Data.unitsPerJob   = 200
config.Data.totalUnits = 10000
config.Data.ignoreLocality = True

submit(config)
