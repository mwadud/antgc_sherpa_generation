CRAB project directory:		/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0038_ptmin100_ptmax200/crab_h3z0p0038_ptmin100_ptmax200_jjg/
Task name:			181024_160715:mwadud_crab_h3z0p0038_ptmin100_ptmax200_jjg
Grid scheduler - Task Worker:	crab3@vocms0198.cern.ch - crab-prod-tw01
Status on the CRAB server:	SUBMITTED
Task URL to use for HELP:	https://cmsweb.cern.ch/crabserver/ui/task/181024_160715%3Amwadud_crab_h3z0p0038_ptmin100_ptmax200_jjg
Dashboard monitoring URL:	http://dashb-cms-job.cern.ch/dashboard/templates/task-analysis/#user=mwadud&refresh=0&table=Jobs&p=1&records=25&activemenu=2&status=&site=&tid=181024_160715%3Amwadud_crab_h3z0p0038_ptmin100_ptmax200_jjg
Warning:			Task requests 4000 MB of memory, but only 2500 MB are guaranteed to be available. Jobs may not find a site where to run and stay idle forever.
Status on the scheduler:	SUBMITTED

Jobs status:                    finished      		  98.0% (49/50)
				running         2.0% ( 1/50)

Publication status:		done           98.0% (49/50)
(from CRAB internal bookkeeping in transferdb)
				unsubmitted     2.0% ( 1/50)

Output dataset:			/aNTGC_h3z0p0038_ptmin100_ptmax200_jjg_24Oct2018/mwadud-aNTGC_h3z0p0038_ptmin100_ptmax200_jjg_24Oct2018-d55eeb9c57c00c385a4a73d8d4c8aadd/USER
Output dataset DAS URL:		https://cmsweb.cern.ch/das/request?input=%2FaNTGC_h3z0p0038_ptmin100_ptmax200_jjg_24Oct2018%2Fmwadud-aNTGC_h3z0p0038_ptmin100_ptmax200_jjg_24Oct2018-d55eeb9c57c00c385a4a73d8d4c8aadd%2FUSER&instance=prod%2Fphys03

Warning: the max jobs runtime is less than 30% of the task requested value (1250 min), please consider to request a lower value (allowed through crab resubmit) and/or improve the jobs splitting (e.g. config.Data.splitting = 'Automatic') in a new task.

Summary of run jobs:
 * Memory: 2049MB min, 3988MB max, 2183MB ave
 * Runtime: 1:18:04 min, 3:29:17 max, 2:04:01 ave
 * CPU eff: 85% min, 99% max, 96% ave
 * Waste: 1:52:21 (2% of total)

Log file is /afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/jjg/h3z0p0038_ptmin100_ptmax200/crab_h3z0p0038_ptmin100_ptmax200_jjg/crab.log
