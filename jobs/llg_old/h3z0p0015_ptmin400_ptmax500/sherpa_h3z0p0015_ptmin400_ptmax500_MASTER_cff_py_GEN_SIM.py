# Auto generated configuration file
# using: 
# Revision: 1.19 
# Source: /local/reps/CMSSW/CMSSW/Configuration/Applications/python/ConfigBuilder.py,v 
# with command line options: sherpa_h3z0p0015_ptmin400_ptmax500_MASTER_cff.py --mc -s GEN,SIM -n 10000 --fileout sherpa_h3z0p0015_ptmin400_ptmax500_MASTER_cff_py_GENSIM.root --conditions 94X_mc2017_realistic_v12 --eventcontent RAWSIM --datatier GEN-SIM --era Run2_2017 --customise Configuration/DataProcessing/Utils.addMonitoring --no_exec
import FWCore.ParameterSet.Config as cms

from Configuration.StandardSequences.Eras import eras

process = cms.Process('SIM',eras.Run2_2017)

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('Configuration.EventContent.EventContent_cff')
process.load('SimGeneral.MixingModule.mixNoPU_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.Geometry.GeometrySimDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.Generator_cff')
process.load('IOMC.EventVertexGenerators.VtxSmearedRealistic50ns13TeVCollision_cfi')
process.load('GeneratorInterface.Core.genFilterSummary_cff')
process.load('Configuration.StandardSequences.SimIdeal_cff')
process.load('Configuration.StandardSequences.EndOfProcess_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(10000)
)

# Input source
process.source = cms.Source("EmptySource")

process.options = cms.untracked.PSet(

)

# Production Info
process.configurationMetadata = cms.untracked.PSet(
    annotation = cms.untracked.string('sherpa_h3z0p0015_ptmin400_ptmax500_MASTER_cff.py nevts:10000'),
    name = cms.untracked.string('Applications'),
    version = cms.untracked.string('$Revision: 1.19 $')
)

# Output definition

process.RAWSIMoutput = cms.OutputModule("PoolOutputModule",
    SelectEvents = cms.untracked.PSet(
        SelectEvents = cms.vstring('generation_step')
    ),
    compressionAlgorithm = cms.untracked.string('LZMA'),
    compressionLevel = cms.untracked.int32(9),
    dataset = cms.untracked.PSet(
        dataTier = cms.untracked.string('GEN-SIM'),
        filterName = cms.untracked.string('')
    ),
    eventAutoFlushCompressedSize = cms.untracked.int32(20971520),
    fileName = cms.untracked.string('sherpa_h3z0p0015_ptmin400_ptmax500_MASTER_cff_py_GENSIM.root'),
    outputCommands = process.RAWSIMEventContent.outputCommands,
    splitLevel = cms.untracked.int32(0)
)

# Additional output definition

# Other statements
process.genstepfilter.triggerConditions=cms.vstring("generation_step")
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, '94X_mc2017_realistic_v12', '')

process.generator = cms.EDFilter("SherpaGeneratorFilter",
    FetchSherpack = cms.bool(False),
    SherpaDefaultWeight = cms.double(1.0),
    SherpaParameters = cms.PSet(
        MPI_Cross_Sections = cms.vstring(' MPIs in Sherpa, Model = Amisic:', 
            ' semihard xsec = 38.95 mb,', 
            ' non-diffractive xsec = 17.0318 mb with nd factor = 0.3142.'),
        Run = cms.vstring(' (run){', 
            ' EVENTS 10000; ERROR 0.1;', 
            ' FSF:=1.; RSF:=1.; QSF:=1.;', 
            ' SCALES STRICT_METS{FSF*MU_F2}{RSF*MU_R2}{QSF*MU_Q2};', 
            ' ME_SIGNAL_GENERATOR Comix;', 
            ' EVENT_GENERATION_MODE PartiallyUnweighted;', 
            ' BEAM_1 2212; BEAM_ENERGY_1 6500.;', 
            ' BEAM_2 2212; BEAM_ENERGY_2 6500.;', 
            ' MODEL NTGC;', 
            ' MASSIVE[15] 1;', 
            ' RANDOM_SEED1=5092', 
            ' RANDOM_SEED2=1751', 
            ' RANDOM_SEED3=2029', 
            ' RANDOM_SEED4=9177', 
            ' PARTICLE_CONTAINER 900[m:-1] gammaZ 22 23;', 
            '}(run)', 
            ' (processes){', 
            ' Process 93 93 -> 900[a] 22;', 
            ' Decay 900[a] -> 90 90;', 
            ' Order (*,*,*);', 
            ' Print_Graphs Process;', 
            ' Integration_Error 0.02;', 
            ' End process', 
            '}(processes)', 
            ' (selector){', 
            ' DecayMass 900 30.0 E_CMS;         # decay products of "900" (defined above) shall inv. mass > 2 GeV', 
            ' IsolationCut 22 0.4 1 0.1;       # Frixione isolated photons with R=0.4, n=1, eps=0.1', 
            ' ET  22  400 500;                 # require the isolated photons', 
            ' DeltaR 93 22 0.4 10.;            # dR(j,P)>0.4', 
            ' PseudoRapidity 22 -2.6 2.6       # |eta|<2.6', 
            '}(selector)', 
            ' (ufo){', 
            ' block mass', 
            ' 23 91.1876     # MZ', 
            ' 15 1.777       # MTA', 
            ' 6  172         # MT', 
            ' 5  4.7         # MB', 
            ' 25 125         # MH', 
            ' block dim8', 
            ' 1  5.0306           # CBtWL4', 
            ' 2  0           # CBWL4', 
            ' 3  0           # CWWL4', 
            ' 4  0           # CBBL4', 
            ' block sminputs', 
            ' 1  127.9       # aEWM1', 
            ' 2  1.16637e-05 # Gf', 
            ' 3  0.1184      # aS', 
            ' block yukawa', 
            ' 5  4.7         # ymb', 
            ' 6  172         # ymt', 
            ' 15 1.777       # ymtau', 
            ' decay \t23 2.4952      # WZ', 
            ' decay \t24 2.085       # WW', 
            ' decay \t6  1.50833649  # WT', 
            ' decay \t25 0.00407     # WH', 
            '}(ufo)'),
        parameterSets = cms.vstring('MPI_Cross_Sections', 
            'Run')
    ),
    SherpaPath = cms.string('./'),
    SherpaPathPiece = cms.string('./'),
    SherpaProcess = cms.string('h3z0p0015_ptmin400_ptmax500'),
    SherpaResultDir = cms.string('Result'),
    SherpackChecksum = cms.string('16a05a265586ba2ffc9776e33ee5d664'),
    SherpackLocation = cms.string('./'),
    crossSection = cms.untracked.double(-1),
    filterEfficiency = cms.untracked.double(1.0),
    maxEventsToPrint = cms.int32(0)
)


process.ProductionFilterSequence = cms.Sequence(process.generator)

# Path and EndPath definitions
process.generation_step = cms.Path(process.pgen)
process.simulation_step = cms.Path(process.psim)
process.genfiltersummary_step = cms.EndPath(process.genFilterSummary)
process.endjob_step = cms.EndPath(process.endOfProcess)
process.RAWSIMoutput_step = cms.EndPath(process.RAWSIMoutput)

# Schedule definition
process.schedule = cms.Schedule(process.generation_step,process.genfiltersummary_step,process.simulation_step,process.endjob_step,process.RAWSIMoutput_step)
from PhysicsTools.PatAlgos.tools.helpers import associatePatAlgosToolsTask
associatePatAlgosToolsTask(process)
# filter all path with the production filter sequence
for path in process.paths:
	getattr(process,path)._seq = process.ProductionFilterSequence * getattr(process,path)._seq 

# customisation of the process.

# Automatic addition of the customisation function from Configuration.DataProcessing.Utils
from Configuration.DataProcessing.Utils import addMonitoring 

#call to customisation function addMonitoring imported from Configuration.DataProcessing.Utils
process = addMonitoring(process)

# End of customisation functions

# Customisation from command line

# Add early deletion of temporary data products to reduce peak memory need
from Configuration.StandardSequences.earlyDeleteSettings_cff import customiseEarlyDelete
process = customiseEarlyDelete(process)
# End adding early deletion
