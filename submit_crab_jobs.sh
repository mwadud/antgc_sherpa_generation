#!/bin/bash

channel=jjg
step=2

jobdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/${channel}
# sherpaDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/aNTGCeventGeneration_mine/test/
# cmsswDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/Configuration/Generator/python/
submitdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/

crab_cfg_template=${submitdir}/crab_submit.py



h3z=( 0 0.0038 0.0029 0.0015 0.0008 )
# h3z=( -1 -0.0038 -0.0029 -0.0015 -0.0008 )
ptmin=( 1 10 50 80 100 200 300 400 500 800)
ptmax=( 10 50 80 100 200 300 400 500 800 1000)

nsamples=100
nevents=10000



# for j in {0..9}
for j in 2
do
	ptmin_val=${ptmin[${j}]}
	ptmax_val=${ptmax[${j}]}

	echo "pt bin : "${ptmin_val}"-"${ptmax_val} "GeV"

	# for i in {0..4}
	for i in 0
	do
		# if [ $i -eq 3 ] && [ $j -eq 2 ];
		# then
		# 	continue
		# fi

		h3z_val=${h3z[${i}]}
		echo "       " $h3z_val

		couplingname=h3z${h3z_val}_ptmin${ptmin_val}_ptmax${ptmax_val}
		couplingname=$(sed "s/\./p/g" <<< ${couplingname})
		coupling_job_dir=${jobdir}/${couplingname}/
		status_file=${jobdir}/${couplingname}/crab_step1_status_${couplingname}.txt
		crab_cfg_file=${coupling_job_dir}/crab_submit_step${step}_${couplingname}_${channel}.py
		psetname=sherpa_${couplingname}_step${step}_cff.py
		jobname=${couplingname}_${channel}_MINIAODSIM_retry


		#grab dataset name
		if [ ${step} -eq 2 ]
		then
			dsetname=$(grep "Output dataset:" ${status_file})
			dsetname=$(echo $dsetname | sed "s/Output dataset://g")
			dsetname=$(echo $dsetname | sed "s/ //g")
			echo "              " $dsetname
			if [ -z "$dsetname" ]
			then
				echo "               Dataset name not found!"
				continue
			fi
		fi

		pluginname='Analysis'
		splitting='EventAwareLumiBased'
		# splitting='Automatic'
		inputdbs='phys03'


		if [ ${step} -eq 1 ]
		then
				pluginname='PrivateMC'
				splitting='EventBased'
				inputdbs='global'
				jobname=${couplingname}_${channel}
		fi

		cp ${crab_cfg_template} ${crab_cfg_file}
		sed -i 's|#workarea|'$coupling_job_dir'|g' ${crab_cfg_file}
		sed -i 's|#jobname|'$jobname'|g' ${crab_cfg_file}
		sed -i 's|#psetname|'$psetname'|g' ${crab_cfg_file}
		sed -i 's|#couplingname|'$couplingname'|g' ${crab_cfg_file}
		if [ ${step} -eq 2 ]
		then
			sed -i 's|#config.Data.inputDataset|'config.Data.inputDataset'|g' ${crab_cfg_file}
			sed -i 's|#inputDataset|'${dsetname}'|g' ${crab_cfg_file}
		fi
		sed -i 's|#pluginname|'${pluginname}'|g' ${crab_cfg_file}
		sed -i 's|#splitting|'${splitting}'|g' ${crab_cfg_file}
		sed -i 's|#inputdbs|'${inputdbs}'|g' ${crab_cfg_file}

		if [ ${step} -eq 1 ]
			then
				sed -i 's|#config.Data.outputPrimaryDataset|'config.Data.outputPrimaryDataset'|g' ${crab_cfg_file}
				sed -i 's|#config.JobType.eventsPerLumi|'config.JobType.eventsPerLumi'|g' ${crab_cfg_file}				
		fi

		cd ${coupling_job_dir}

		python ${crab_cfg_file}

		cd -
	done
done

echo "Submission complete!"
