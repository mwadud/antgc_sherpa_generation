#!/bin/bash

channel=jjg
step=1

jobdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/jobs/${channel}
# sherpaDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/aNTGCeventGeneration_mine/test/
# cmsswDir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/Configuration/Generator/python/
submitdir=/afs/cern.ch/work/m/mwadud/private/naTGC/CMSSW_9_4_0_patch1/src/GeneratorInterface/SherpaInterface/crab_job/

crab_cfg_template=${submitdir}/crab_submit.py



h3z=( -1 0.0038 0.0029 0.0015 0.0008 0 )
 # h3z=( -0.0038 -0.0029 -0.0015 -0.0008 )
ptmin=( -1 1 10 50 80 100 200 300 400 500 800)
ptmax=( -2 10 50 80 100 200 300 400 500 800 1000)


nsamples=100
nevents=10000



for j in {1..10}
do
	ptmin=${ptmin[${j}]}
	ptmax=${ptmax[${j}]}

	echo "pt bin : "${ptmin}"-"${ptmax} "GeV"

	for i in {1..5}
	do
		h3z_val=${h3z[${i}]}
		echo "	" $h3z_val

		couplingname=h3z${h3z[${i}]}_ptmin${ptmin}_ptmax${ptmax}
		couplingname=$(sed "s/\./p/g" <<< ${couplingname})
		coupling_job_dir=${jobdir}/${couplingname}/crab_${couplingname}_${channel}/
		status_file=${jobdir}/${couplingname}/crab_step${step}_status_${couplingname}.txt

		crab status -d ${coupling_job_dir} > ${status_file}

		dsetname=$(grep "Output dataset:" ${status_file})
		echo "              " $dsetname
#		crab status -d ${coupling_job_dir}
	done
done

echo "Task complete!"
